from django.shortcuts import render
from app.models import Profesor
from django.http import HttpResponse


# Create your views here.
def home(request):
    template_name = 'perfil.html'
    data = {} 
    data['profesores'] = Profesor.objects.all().order_by('id')

    return render(request, template_name, data)
