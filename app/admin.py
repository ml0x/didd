from django.contrib import admin
from app.models import Facultad, Campus, Clase, Profesor


@admin.register(Facultad)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'campus','estado']
    list_filter = ('campus', 'estado')

@admin.register(Campus)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['nombre', 'estado']
    list_filter = ('estado', )


@admin.register(Clase)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['nrc', 'nombre', 'campus', 'facultad', 'estado']
    list_filter = ('campus', 'facultad', 'estado', )
    pass

@admin.register(Profesor)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['rut', 'nombre', 'apellido_paterno','apellido_materno', 'correo', 'campus', 'facultad', 'estado']
    list_filter = ('campus', 'facultad', 'estado', )
    pass


"""
@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name', 'group_client']
    list_filter = ('group_client', )

@admin.register(GroupClient)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']
    pass

@admin.register(TypeService)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']
    pass

@admin.register(LocalisationCountry)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']
    pass

@admin.register(LocalisationZone)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']
    pass

@admin.register(LocalisationState)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']
    pass

@admin.register(Area)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']
    pass

@admin.register(ProfilCenter)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']
    

@admin.register(Service)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['name']

@admin.register(ClientService)
class ClientAdmin(admin.ModelAdmin):
    list_display = ['service']
"""
