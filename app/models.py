from django.db import models


class Campus(models.Model):
    nombre = models.CharField(max_length=144)
    estado = models.BooleanField(default=True)
    def __str__(self):
        return self.nombre

class Facultad(models.Model):
    nombre = models.CharField(max_length=144)
    campus = models.ForeignKey(Campus, on_delete=models.CASCADE)
    estado = models.BooleanField(default=True)
    def __str__(self):
        return self.nombre

class Clase(models.Model):
    nrc = models.CharField(max_length=5)
    nombre = models.CharField(max_length=144)
    facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)
    campus = models.ForeignKey(Campus, on_delete=models.CASCADE)
    estado = models.BooleanField(default=True)
    def __str__(self):
        return self.nombre

class Profesor(models.Model):
    rut = models.CharField(max_length=10)
    nombre = models.CharField(max_length=144)
    apellido_paterno = models.CharField(max_length=144)
    apellido_materno = models.CharField(max_length=144)
    correo = models.EmailField()
    campus = models.ForeignKey(Campus, on_delete=models.CASCADE)
    facultad = models.ForeignKey(Facultad, on_delete=models.CASCADE)
    estado = models.BooleanField(default=True)
    def __str__(self):
        return self.nombre




"""
class GroupClient (models.Model):
    name = models.CharField(max_length=144)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TypeService(models.Model):
    name = models.CharField(max_length=144)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class LocalisationCountry(models.Model):
    name = models.CharField(max_length=144)
    iso_code_2 = models.CharField(max_length=2)
    iso_code_3 = models.CharField(max_length=3)
    sort_order = models.IntegerField()
    status = models.BooleanField(default=True)

    def __str__(self):
        return self.name

class LocalisationZone(models.Model):
    name = models.CharField(max_length=144)
    code = models.CharField(max_length=32)
    sort_order = models.IntegerField()
    status = models.BooleanField(default=True)
    localisation_country = models.ForeignKey(LocalisationCountry, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class LocalisationState(models.Model):
    name = models.CharField(max_length=144)
    sort_order = models.IntegerField()
    status = models.BooleanField(default=True)
    localisation_zone = models.ForeignKey(
        LocalisationZone, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Area(models.Model):
    name = models.CharField(max_length=144)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class ProfilCenter(models.Model):
    name = models.CharField(max_length=144)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class Client(models.Model):
    name = models.CharField(max_length=144)
    business_name = models.CharField(max_length=144)
    address_1 = models.CharField(max_length=144)
    identification_type = models.CharField(max_length=1)
    identification_number = models.CharField(max_length=50)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    group_client = models.ForeignKey(GroupClient, on_delete=models.CASCADE)
    localisation_country = models.ForeignKey(LocalisationCountry, on_delete=models.CASCADE)
    localisation_state = models.ForeignKey(LocalisationState, on_delete=models.CASCADE)
    localisation_zone = models.ForeignKey(LocalisationZone, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Service(models.Model):
    name = models.CharField(max_length=144)
    comment = models.CharField(max_length=1000)
    create_at = models.DateTimeField(auto_now_add=True)
    modified_at = models.DateTimeField(auto_now_add=True)
    area = models.ForeignKey(Area, on_delete=models.CASCADE)
    profil_Center = models.ForeignKey(ProfilCenter, on_delete=models.CASCADE)
    type_service = models.ForeignKey(TypeService, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class ClientService(models.Model):
    amount = models.IntegerField()
    pay_day = models.IntegerField()
    service = models.ForeignKey(Service, on_delete=models.CASCADE)
    profil_Center = models.ForeignKey(ProfilCenter, on_delete=models.CASCADE)
"""
